# Screenshot automation with Cypress

> Demo of how to automate taking screenshots of NRK news articles

![figure](screenshot.png "Cypress screenshot automation")

## Install

```shell
npm i
```

- Article URLs are listed in `/cypress/fixtures/urls.json` file
- Screenshots will be stored under `/cypress/screenshots` directory

## Run

```shell
npm run cy:run
```

## Develop

```shell
npm run cy:open
```
