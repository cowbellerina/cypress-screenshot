import articles from '../fixtures/urls.json'

describe('NRK news article screenshots', () => {
  beforeEach(() => {
    cy.setCookie('nrkno-cookie-information', '1')
  })

  for (let index = 0; index < articles.length; index++) {
    const url = articles[index]
    const testName = url.replace('https://www.nrk.no/', '')
    const fileName = `${index}-${url.split('/').pop()}`

    it(testName, () => {
      cy.visit(url, {failOnStatusCode: false})
        .then(() => {
          cy.get('article[role=main]')
            .screenshot(fileName, {
              padding: 10,
              overwrite: true,
              timeout: 10_000
            })
        })
    })
  }
})
